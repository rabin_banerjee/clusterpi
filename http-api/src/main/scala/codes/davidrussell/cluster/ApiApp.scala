package codes.davidrussell.cluster

import akka.actor.{ActorSystem, Address}
import akka.cluster.Cluster
import akka.cluster.ddata.{DistributedData, LWWMapKey, PNCounterMapKey, Replicator}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.routing.FromConfig
import akka.stream.ActorMaterializer
import akka.util.Timeout
import codes.davidrussell.cluster.Messages._
import com.typesafe.config.ConfigFactory

import scala.collection.immutable._
import scala.concurrent.duration._

// TODO: Make the routes actually RESTful.
object ApiApp extends App {

  val seedNode = args(0)
  val seedPort = if (args.length > 1) args(1).toInt else 2552

  val config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + "localhost").
    withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + 8221)).
    withFallback(ConfigFactory.load())

  implicit val system = ActorSystem("ClusterSystem", config)
  val seedAddress = Address("akka.tcp", "ClusterSystem", seedNode, seedPort)

  Cluster(system).joinSeedNodes(Seq(seedAddress))

  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val backendRouter = system.actorOf(FromConfig.props(), name = "router")
  implicit val timeout = Timeout(20 seconds)

  val replicator = DistributedData(system).replicator
  val modeMapKey = LWWMapKey[String]("mode")
  val voteMapKey = PNCounterMapKey("voteMap")

  replicator ! Replicator.Subscribe(modeMapKey, backendRouter)
  replicator ! Replicator.Subscribe(voteMapKey, backendRouter)

  val route =
    path("") {
      get {
        complete {
          val clientAppStream = getClass.getResourceAsStream("/client-app.html")
          val lines = scala.io.Source.fromInputStream(clientAppStream).mkString
          HttpResponse(entity = HttpEntity(ContentTypes.`text/html(UTF-8)`, lines))
        }
      }
    } ~
      path("admin") {
        get {
          complete {
            val adminAppStream = getClass.getResourceAsStream("/admin.html")
            val lines = scala.io.Source.fromInputStream(adminAppStream).mkString
            HttpResponse(entity = HttpEntity(ContentTypes.`text/html(UTF-8)`, lines))
          }
        }
      } ~
      path("vote") {
        get {
          parameters("color".as[Int]) { (color) =>
            complete {
              (backendRouter ? Vote(color)).map[ToResponseMarshallable] {
                case Result(result) =>
                  "Voted: " + result
              }
            }
          }
        }
      } ~
      path("blink") {
        get {
          parameters("in".as[Int]) { (n) =>
            complete {
              (backendRouter ? Blink(n)).map[ToResponseMarshallable] {
                case Result(result) =>
                  "Result: " + result
              }
            }
          }
        }
      } ~
      path("voteMode") {
        get {
          complete {
            (backendRouter ? SetMode(VotingMode())).map[ToResponseMarshallable] {
              case Result(result) =>
                result
            }
          }
        }
      } ~
      path("voteReset") {
        get {
          complete {
            (backendRouter ? VoteReset).map[ToResponseMarshallable] {
              case Result(result) =>
                result
            }
          }
        }
      } ~
      path("leaderMode") {
        get {
          complete {
            (backendRouter ? SetMode(LeaderMode())).map[ToResponseMarshallable] {
              case Result(result) => result
            }
          }
        }
      } ~
      path("gossipMode") {
        get {
          complete {
            (backendRouter ? SetMode(GossipMode())).map[ToResponseMarshallable] {
              case Result(result) => result
            }
          }
        }
      } ~
      path("blinkMode") {
        get {
          complete {
            (backendRouter ? SetMode(BlinkMode())).map[ToResponseMarshallable] {
              case Result(result) => result
            }
          }
        }
      }

  val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

}