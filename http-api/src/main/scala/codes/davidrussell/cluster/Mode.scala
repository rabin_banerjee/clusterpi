package codes.davidrussell.cluster

trait Mode {
  val modeName: String
}

final case class GossipMode() extends Mode {
  override val modeName = "Gossip"
}

final case class VotingMode() extends Mode {
  override val modeName: String = "Voting"
}

final case class LeaderMode() extends Mode {
  override val modeName: String = "Leader"
}

final case class BlinkMode() extends Mode {
  override val modeName: String = "Blink"
}
