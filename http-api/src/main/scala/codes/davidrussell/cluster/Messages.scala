package codes.davidrussell.cluster

object Messages {
  final case class Result(n: String)
  final case class SetMode(mode: Mode)
  final case class Vote(color: Int)
  final case class Blink(n: Int)
  case object VoteReset
}
