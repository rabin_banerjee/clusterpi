# A Literally-Hands-On Introduction to Distributed Systems with Akka Cluster

This is a project to demo some of the features of Akka Cluster

## Quick Start

1. navigate to the scripts directory, and update the `deploy-targets.txt` file with your own pi's bonjour hostnames.
2. for ease of deployment it might be handy to add an RSA key to your pi's authorized hosts, since the deploy scripts use scp and ssh
3. create a `projects` directory in your pis' home folders. (i.e. `~/projects`)
3. run `./deploy.sh` - this will :
  - package up the cluster and http applications
  - deploy the cluster application to each of the deploy targets
  - provide each target with a run.sh script in the home directory
  - provide a helper script to find the pi's ip address
4. ssh into your pi's
5. on any of the pi's:
  - run `echo $(./ip.sh)` <-- this will be your seed node ip address
  - run `sudo ./run.sh -s $(./ip.sh eth0)` <-- this will run your seed node.
6. on the rest of the pi's:
  - run `sudo ./run.sh -s THE_SEED_IP_HERE`

## Prerequisites

1. Raspberry Pis running on the same network. (this project will work with all of them up to Pi 3B) 
2. 3 LEDs, connected to your pi as pictured below.
3. to simulate a network partition you can attach a few pi's to a couple of switches.
4. if you do #3, then you'll need ethernet cables.

  - ![Raspberry Pi pin configuration](pin-config.jpg "Raspberry Pi pin configuration")

