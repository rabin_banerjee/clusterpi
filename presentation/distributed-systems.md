build-lists: true

![left, fit](test-projector.gif)

# [fit] *This* is __*a*__ test __slide__

^ START SEED NODE AND HTTP NODE
___

![](different.jpg)

^ there's been a lot of great presentations today, but I thought I'd present on something a bit different...

---

# [fit] __Generic Type__ Programming
# [fit] with *Streaming* and *Kafka*
#### ... With Microservices

^ just kidding.

---

![left,fit](kixeye-logo.png)

## Dave Russell
## __*@davidgraig*__

^ Senior full-stack engineer working at KIXEYE in Victoria with our Scala game servers. We make "midcore" RTS games.

---

# [fit] *A Literally-Hands-On Introduction to*
# [fit] __Distributed Systems__
# [fit] *with* Akka Cluster

^ Context: This submission of this talk started as a bit of a troll: I had only a cursory knowledge of distributed systems and in particular 0 knowledge of akka cluster. "oh no it got accepted". This talk is for those of us that don't know much about distributed systems, but you really really need to before you have to give a presentation.

---

![](small-bike.gif)

^ Let's get this out of the way - akka cluster on a raspberry pi is a bit like a regular sized man riding a little bike. It's hilarious, and interesting

---

# [fit] Agenda

---

# [fit] Agenda
### __Raspberry Pis__

---

# [fit] Agenda
### __Raspberry Pis__
### Distributed Systems

---

# [fit] Agenda
### __Raspberry Pis__
### Distributed Systems
### __Akka Cluster__

---

![](pi.jpg)
# [fit] *This is a* __Raspberry Pi__

^ _Leader Mode Pulse one raspberry pi while explaining_

---

![](pi.jpg)

# [fit] It's small
# [fit] It runs __Linux__ *(kinda)*
# [fit] It has GPIO pins

^ - Raspberry pi's were built to be a solid tinkering platform for exploration

^ - USB ports, HDMI, network, etc.

---

# [fit] This **is** a **distributed** system

^ - Distributed systems are collections of smaller discrete functional units, which work in concert to achive a higher purpose.

^ - Distributed systems by definition don't usually exist in one room. They trade simplicity for performance at scale. 

---
# [fit] This
---
# [fit] **is**
---
# [fit] a
---
# [fit] **distributed**
---
# [fit] system
---

# ...

^ more often, the system might look like this (perhaps a bit of overlap in the responsibilites between services)

---
# [fit] Th
---
# [fit] is **is**
---
# [fit] a **di**
---
# [fit] **strib**
---
# [fit] **uted**
---
# [fit] sys
---
# [fit] tem
---

# ...

^ MOST often, the system will not be easy to reason about and see from 1000 feet.

---
# [fit] Thi
---
# [fit] **s** a **di**
---
# [fit] tem
---
# [fit] s **i**
---
# [fit] **strib**
---
# [fit] **uted**
---
# [fit] sys
---

# ... 

^ Distributed systems are here, and they're not going away.

^ include RESTful API's, mobile (client/server), replicated databases, cloud services

^ needed to overcome latency, number of users, and reliability

^ needed to provide consistency between nodes 
(when a rich/stateful client needs to update the system, how do you merge that state, and then continue on?)

^ are hard.

---
### [fit] *Good presentations make* 
# [fit] __large things__ 
### *seem* __*small*__

^ _Change to Gossip Mode and bring up the rest of the cluster_

^ Raspberry Pi's offer a way to model a distributed system such that you can see the whole thing literally in one place. (Plus, y'know, they have lights... so it's FUN)

---

# [fit] Theory

^ Jonas Bonér covers the following in MUCH greater detail in "The Road to Akka Cluster, And Beyond", I'm going to provide very reductionist overview of this.

---

# [fit] *8* 
# [fit] Fallacies of Distributed Systems

^ L Peter Deutsch formalized 8 common mistakes developers make when working with distributed systems

^ 8 would be hard to remember, so...

---

# [fit] __*4*__ 
# [fit] Fallacies of Distributed Systems

---

## __Secure__

^ There are bad people, so Sanitize your inputs.

---

## __Reliable__ 

^ There are bad programmers, So plan on the network being unavailable.

---

## __No Latency__

^ There is a thing called the speed of light, with possibly other services between your services and the client.

---

## __∞ bandwidth__

^ Has anyone accidentally swallowed an ice cube whole? Don't do that to your systems. Just send the deltas.

---

# [fit] FLP

### Fischer, Lynch, and Paterson

^ That covers the 8 falacies - let's talk about

^ Fischer, Lynch, and Paterson (1985)

---

#### (a.k.a Impossibility of Distributed Concensus with One Faulty Process)

# In a distributed system

- We can't tell if a process is dead, or just delayed

- So, it's impossible to guarauntee that all nodes in a system have the same value (i.e. Concencus)

^ _Demo: blink 300 times, 5 times, change to vote mode and vote red._

---

![fit](not-dead-yet.gif)

---

# [fit] CAP

### Eric Brewer, Lynch and Gilber

^ fast forward 15 years

^ Conjecture by Eric Brewer (2000), Proof by Lynch and Gilbert in 2002

---
## Consistency, Availability, Partition Tolerance

- Reads from the system might not return the last completed write

^ Choose 2:

^ C: The system behaves as one cohesive unit.

^ A: The system will respond to every request.

^ P: The sytem continues to work in spite of network failures  

![inline fit](cap.png)

---

# Strong Consistency

^ Hard to do responsively in a distributed system 

- Jonas Bonér: Most SQL DBs __*SAY*__ they provide strong consistency, but __*implement weaker guarantees*__.

- *Highly Available Transactions* (Peter Bailis 2013)

- *SAGA Pattern* (Thanks Michael Nash!)

^ Some of these are possible to replicate in a distributed system! Highly Available Transactions

^ read un/committed, read your writes, monotonic atomic view, monotonic read/write

---

![](time.jpg)

# [fit] __Time__

^ A quick note on time: physical clocks drift, but causal clocks don't

^ Vector clocks keep a history of causality, but it carries a lot of state around.

^ basically "he said she said"

^ Basho gives a really good introduction to vector clocks: http://basho.com/posts/technical/why-vector-clocks-are-easy/

---

# [fit]  __Failure__ detection

^ Remember FLP? A good failure detector is skilled at guessing if the node is down.

---

- __PHI__: Accrual detection 
- __SWIM__: Delegated detection 

^ PHI: keeps response times history from heartbeat requests, uses heuristic (PHI) to guess if a node has failed.

^ SWIM: if __A__ can't hit __B__, send a request to __C__, __D__, and __E__ to hit __B__, then report back to __A__.

---

# [fit] Consensus Protocols

---

# Strongly Consistent

^ Paxos: hard to understand, hard to implement the spec in the real world (which leads to be unproven)

^ ZAB: has proof of correctness

^ Raft: simple(r?) than Paxos

- Paxos 
- ZAB (Zookeeper Atomic Broadcast)
- [Raft](http://thesecretlivesofdata.com/raft/)

---

# [fit] CRDTs

---

# [fit] Conflict Free Data Types

- *Convergent* are __state-based__
- *Commutative* require a __broadcast channel__

^ Requires a reliable broadcast channel to broadcast state change, which is then applied locally

^ if there is a network partition and shared state needs to be merged, these will do so without manual intervention

---

![](gossip.jpg)
# [fit] __Gossip__ Protocols
___

![](gossip.jpg)
# Gossip Protocols
- Tell neighbor nodes about changes in shared state.

^ resilient, peer oriented

---

# [fit] Akka __*Cluster*__

^ Toolkit wrapping an Actor Model (send messages back and for with immutable copies of shared state)
^ Actor model is great for concurrency and distributed systems
^ Akka Remoting allows actors to live on a remote system
^ Akka Cluster is a membership service to abstract actor systems as nodes, and manage them.

^ 2400 nodes in a cluster
^ 1000 nodes reach convergence(??) in 4 minutes.

---

# [fit] How does it __*gossip*__?

^ Alternatives: could use ZooKeeper, Serf
^ based on Dynamo and Riak, there is a preference given to nodes that haven't seen the latest version

---

![fit](gossip-1.png)

---

![fit](gossip-2.png)

---

![fit](gossip-3.png)

---

![fit](gossip-4.png)

---

![fit](gossip-5.png)

---

![fit](gossip-6.png)

^ Biased, Push/Pull Gossip (gzipped vector clock push/pull to 80% of cluster up to 400 nodes)
^ vector clocks are pruned as of 2.4

---

# [fit] How does it detect __*failure*__?

^ could use Sensu monitoring + hand rolled, ZK, etcd, etc.

---

![fit](failure-1.png)

^ monitors up to 5 neighbors

---

![fit](failure-2.png)

^ if any monitoring node doesn't get a response, it will gossip that the subject should be unreachable

---

![fit](failure-3.png)

---

![fit](failure-4.png)

---

![fit](failure-5.png)

^ quarantine nodes must be downed (either automatically, or manually via JMX/CLM tools)

---

# [fit] How does it make __*decisions*__?

^ a node can be Joining, Up, Leaving, Exiting, Down, Removed, and Unreachable

^ The leader node dictates the state of the cluster to the rest of the nodes.

^ WHY CAN'T THE LEADER PERFORM IT'S DUTIES WITHOUT CONVERGENCE?

---

![fit](leader-1.png)

^ F is trying to join the cluster through seed node E

---

![fit](leader-2.png)

---

![fit](leader-3.png)

---

![fit](leader-4.png)

^ Leader election is deterministic!

---

# [fit] How does it handle __*partitions*__?

---

![fit](partition-1.png)

---

![fit](partition-2.png)

---

![fit](partition-3.png)

---

![fit](partition-4.png)

^ talk about split brain

^ talk about options: auto-down, manual intervention, split-brain resolver, do nothing?

---

# [fit] __*Distributed*__ Data

^ experimental as of 2.4

^ supports registers, maps, counters, flags

---

![](brain.gif)

# [fit] Split Brain __*Resolver*__

^ Each partition will acts on the last known state of the cluster

^ Quorum size, Keep Oldest, Majority, Referee (danger!)

^ 2.3 w/ lightbend subscription

---

# Cool stuff

- Cluster Metrics Extension
- Weakly Up (experimental)
- Distributed Pub/Sub
- Cluster Client
- Cluster Singleton
- Cluster Sharding

---

# Code

# [fit] [bitbucket.org/davidgraig/clusterpi](bitbucket.org/davidgraig/clusterpi)

^ application.conf, ClusterApp, WorkerNode

---

# links

## _**The Road To Akka Cluster And Beyond**_
https://www.youtube.com/watch?v=2wSYcyWCtx4

http://raspberrypi.stackexchange.com/questions/1217/what-is-raspbian

https://aphyr.com/posts/313-strong-consistency-models

---