#!/bin/bash

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -s|--seed)
    SEED="$2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

HOST=$(./ip.sh eth0)
/home/pi/projects/cluster-1.0/bin/cluster $HOST 2552 $SEED 2552;