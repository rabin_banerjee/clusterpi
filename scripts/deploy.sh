cd ../cluster
sbt universal:packageBin
cd -
cd ../http-api
sbt universal:packageBin
cd target/universal/ && rm -rf cluster-frontend-1.0 && unzip -q cluster-frontend-1.0.zip
cd ../../../scripts

for dest in $(<deploy-targets.txt); do
  echo ${dest}:
  scp run.sh ${dest}:~/run.sh
  ssh ${dest} 'chmod 777 ~/run.sh'
  scp ip.sh ${dest}:~/ip.sh
  ssh ${dest} 'chmod 777 ~/ip.sh'
  scp ../cluster/target/universal/cluster-1.0.zip ${dest}:~/projects/
  ssh ${dest} 'cd ~/projects && rm -rf cluster-1.0 && unzip -q cluster-1.0.zip'
  ssh ${dest} 'gpio mode 0 OUT && gpio mode 1 OUT && gpio mode 2 OUT'
done