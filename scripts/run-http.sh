#!/bin/bash

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -s|--seed)
    SEED="$2"
    shift # past argument
    ;;
    -h|--host)
    HOST="$2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

../http-api/target/universal/cluster-frontend-1.0/bin/cluster-frontend $SEED 2552;