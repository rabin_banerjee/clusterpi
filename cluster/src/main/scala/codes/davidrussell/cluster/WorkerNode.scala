package codes.davidrussell.cluster

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Props}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent._
import akka.pattern.ask
import akka.util.Timeout
import codes.davidrussell.cluster.DisplayActor._
import codes.davidrussell.cluster.Messages._
import codes.davidrussell.cluster.pi.Pins

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object WorkerNode {
  def props(display: ActorRef, voteActor: ActorRef, modeActor: ActorRef) = Props(new WorkerNode(display, voteActor, modeActor))
}

case class WorkerNode(displayActor: ActorRef, voteActor: ActorRef, modeActor: ActorRef) extends Actor with ActorLogging {

  val cluster = Cluster(context.system)
  implicit val timeout = Timeout(15 seconds)

  override def preStart(): Unit = {
    cluster.subscribe(
      self,
      initialStateMode = InitialStateAsEvents,
      classOf[MemberUp],
      classOf[UnreachableMember],
      classOf[ReachableMember],
      classOf[LeaderChanged],
      classOf[SeenChanged] // NOTE: This Event is from a custom build of Akka Cluster, and is usually private to the akka.cluster namespace
    )
  }

  override def postStop(): Unit = cluster.unsubscribe(self)

  cluster.registerOnMemberRemoved {
    context.system.registerOnTermination(System.exit(0))
    voteActor ! PoisonPill
    context.system.terminate()
  }

  def receive = {
    // Membership events
    case MemberUp(member) =>
      log.info("Member is Up: {}", member.address)
      if (member.address.equals(cluster.selfAddress)) {
        cluster.state.members
        displayActor ! Initialize
      }
      (modeActor ? GetMode).map {
        case mode: VotingMode =>
          voteActor ! VoteCount(self)
      }
    case LeaderChanged(address) =>
      log.info(s"Leader changed to $address")
      (modeActor ? GetMode).map {
        case LeaderMode =>
          displayActor ! Pulse(Set(Pins.Blue))
        case mode: Mode => log.info(s"Leader changed, but mode is: ${mode.modeName}.")
      }
    // Reachability Events
    case UnreachableMember(member) =>
      log.info("Member detected as unreachable: {}", member.address)
    case ReachableMember(member) =>
      log.info("Member detected as reachable: {}", member.address)

    // Internal API Events
    case SeenChanged(convergence, seenBy) =>
      (modeActor ? GetMode).map {
        case mode: GossipMode =>
          log.info("Convergence: {}, Seen By: {} node(s)", convergence, seenBy.size)
          if (seenBy == cluster.state.members)
            displayActor ! FadeOut(500, Set(Pins.Blue))
          else
            displayActor ! FadeOut(250, Set(Pins.Red, Pins.Green))
      }
    // Business Message Handling
    case setMode: SetMode =>
      val replyTo = sender()
      (modeActor ? setMode).map {
        case mode: Mode =>
          replyTo ! Result(s"${mode.modeName} set!")
      }
    case vote: Vote =>
      log.info(s"Received vote: ${vote.color}")
      val replyTo = sender()
      (modeActor ? GetMode).map {
        case voteMode: VotingMode => {
          vote.color match {
            case Pins.Red | Pins.Green | Pins.Blue =>
              (voteActor ? vote).map {
                case voteResult: VoteResult =>
                  replyTo ! Result(getVoteResultsString(voteResult.result))
              }
            case _ => replyTo ! Result("Unknown vote!")
          }
        }
        case _ => replyTo ! Result("Voting isn't enabled.")
      }
    case VoteReset =>
      voteActor ! VoteReset
      sender() ! Result("Reset Votes")
    case blink: Blink => {
      val replyTo = sender()
      (modeActor ? GetMode).map {
        case demoMode: BlinkMode => {
          displayActor ! blink
          replyTo ! Result("Blink sent.")
        }
        case _ => replyTo ! Result("Blinking isn't allowed right now")
      }
    }
  }

  private def getVoteResultsString(results: List[Int]) = {
    s"[R: ${results(Pins.Red)}, G: ${results(Pins.Green)}, B: ${results(Pins.Blue)}]"
  }
}