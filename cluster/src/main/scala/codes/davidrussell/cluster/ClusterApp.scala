package codes.davidrussell.cluster

import akka.actor.{ActorSystem, Address}
import akka.cluster.Cluster
import codes.davidrussell.cluster.pi.{ConsoleLog, IoPins}
import com.typesafe.config.ConfigFactory

import scala.collection.immutable._

object ClusterApp extends App {

  val host = args(0)
  val hostPort = if (args.length > 1) args(1).toInt else 2552
  val seedNode = if (args.length > 2) args(2) else host
  val seedPort = if (args.length > 3) args(3).toInt else hostPort

  val config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + host).
    withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + hostPort)).
    withFallback(ConfigFactory.load())

  implicit val system = ActorSystem("ClusterSystem", config)
  val seedAddress = Address("akka.tcp", "ClusterSystem", seedNode, seedPort)
  Cluster(system).joinSeedNodes(Seq(seedAddress))

  val display = if (host.equals("127.0.0.1")) ConsoleLog() else IoPins()
  val displayActor = system.actorOf(DisplayActor.props(display), name = "displayActor")
  val voteActor = system.actorOf(VoteActor.props(displayActor), name = "voteActor")
  val modeActor = system.actorOf(ModeActor.props(displayActor), name = "modeActor")

  system.actorOf(WorkerNode.props(displayActor, voteActor, modeActor), name = "worker")
}