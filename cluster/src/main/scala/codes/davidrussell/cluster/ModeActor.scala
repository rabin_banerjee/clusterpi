package codes.davidrussell.cluster

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.Replicator._
import akka.cluster.ddata.{DistributedData, LWWMap, LWWMapKey, Replicator}
import codes.davidrussell.cluster.DisplayActor.{Pulse, Reset}
import codes.davidrussell.cluster.Messages.{GetMode, ModeChanged, SetMode}
import codes.davidrussell.cluster.pi.Pins

import scala.concurrent.duration._

object ModeActor {
  def props(displayActor: ActorRef) = Props(new ModeActor(displayActor))
}

case class ModeActor(displayActor: ActorRef) extends Actor with ActorLogging {

  implicit val node = Cluster(context.system)
  val replicator = DistributedData(context.system).replicator
  val ModeMapKey = LWWMapKey[Mode]("modeMap")

  replicator ! Replicator.Subscribe(ModeMapKey, self)

  override def receive: Receive = {
    case SetMode(mode) =>
      log.info(s"set distributed data: ${mode.modeName}")
      val replyTo = sender()
      replicator ! Update(ModeMapKey, LWWMap(), WriteMajority(timeout = 5 seconds))(_ + ("mode" -> mode))
      replicator ! Get(ModeMapKey, ReadMajority(timeout = 5 seconds), request = Some(replyTo))
    case GetMode =>
      val inReplyTo = sender()
      modeMapFor(inReplyTo)
    case changed@Changed(ModeMapKey) =>
      val modeMap = changed.get(ModeMapKey)
      modeMap.get("mode") match {
        case Some(x: Mode) =>
          log.info(s"replicated data changed: ${x.modeName}")
          handleModeChanged(x)
          displayActor ! ModeChanged(x)
        case _ => // nothing
      }

    case success@GetSuccess(ModeMapKey, Some(replyTo: ActorRef)) =>
      val voteMap = success.get(ModeMapKey)
      replyTo ! voteMap.get("mode").get
  }

  private def modeMapFor(replyTo: ActorRef) = {
    val readMajority = ReadMajority(timeout = 5 seconds)
    replicator ! Get(ModeMapKey, readMajority, request = Some(replyTo))
  }

  private def handleModeChanged(mode: Mode) = mode match {
    case leaderMode: LeaderMode if node.state.leader.get.equals(node.selfAddress) => displayActor ! Pulse(Set(Pins.Blue))
    case _ => displayActor ! Reset
  }
}