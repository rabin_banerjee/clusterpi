package codes.davidrussell.cluster

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.cluster.Cluster
import akka.cluster.ddata.Replicator._
import akka.cluster.ddata._
import codes.davidrussell.cluster.DisplayActor.{Reset, SetPins}
import codes.davidrussell.cluster.Messages.{VoteCount, _}
import codes.davidrussell.cluster.pi.Pins

import scala.concurrent.duration._

object VoteActor {
  def props(displayActor: ActorRef) = Props(new VoteActor(displayActor))
}

case class VoteActor(display: ActorRef) extends Actor with ActorLogging {
  val replicator = DistributedData(context.system).replicator
  implicit val node = Cluster(context.system)

  val VoteCounterMapKey = PNCounterMapKey("voteMap")

  replicator ! Replicator.Subscribe(VoteCounterMapKey, self)

  def receive = {
    case Vote(pin) =>
      replicator ! Replicator.Update(VoteCounterMapKey, PNCounterMap.empty, WriteLocal)(x => x.increment(Pins.getText(pin), 1))
      val inReplyTo = sender()
      self ! VoteCount(inReplyTo)
    case VoteCount(replyTo: ActorRef) =>
      replicator ! Get(VoteCounterMapKey, ReadMajority(timeout = 5 seconds), request = Some(replyTo))
    case VoteReset =>
      display ! Reset
      replicator ! Replicator.Update(VoteCounterMapKey, PNCounterMap.empty, WriteMajority(timeout = 5 seconds))(x => x.decrement(Pins.getText(Pins.Red), x.getValue("0").longValue()))
      replicator ! Replicator.Update(VoteCounterMapKey, PNCounterMap.empty, WriteMajority(timeout = 5 seconds))(x => x.decrement(Pins.getText(Pins.Green), x.getValue("0").longValue()))
      replicator ! Replicator.Update(VoteCounterMapKey, PNCounterMap.empty, WriteMajority(timeout = 5 seconds))(x => x.decrement(Pins.getText(Pins.Blue), x.getValue("0").longValue()))
    case changed @ Changed(VoteCounterMapKey) =>
      val voteMap = changed.get(VoteCounterMapKey)
      val results = voteResult(voteMap)
      val maxVal = max(results).get
      val winningPin = results.indexOf(maxVal)
      display ! SetPins(Set(winningPin))

    case success @ GetSuccess(VoteCounterMapKey,  Some(replyTo: ActorRef)) =>
      val voteMap = success.get(VoteCounterMapKey)
      replyTo ! VoteResult(result = voteResult(voteMap))
  }

  private def voteResult(voteMap: PNCounterMap) = {
    val red = Option(voteMap.getValue(Pins.getText(Pins.Red))) match {
      case Some(number) => number.intValue()
      case None => 0
    }
    val green = Option(voteMap.getValue(Pins.getText(Pins.Green))) match {
      case Some(number) => number.intValue()
      case None => 0
    }
    val blue = Option(voteMap.getValue(Pins.getText(Pins.Blue))) match {
      case Some(number) => number.intValue()
      case None => 0
    }
    List(red, green, blue)
  }

  def max(xs: List[Int]): Option[Int] = xs match {
    case Nil => None
    case List(x: Int) => Some(x)
    case x :: y :: rest => max( (if (x > y) x else y) :: rest )
  }
}