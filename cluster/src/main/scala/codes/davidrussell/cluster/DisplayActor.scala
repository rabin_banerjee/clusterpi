package codes.davidrussell.cluster

import akka.actor.{Actor, ActorLogging, Props}
import akka.cluster.Cluster
import codes.davidrussell.cluster.DisplayActor._
import codes.davidrussell.cluster.Messages.Blink
import codes.davidrussell.cluster.pi.{Display, Pins}

import scala.concurrent.ExecutionContext.Implicits.global

object DisplayActor {
  def props(display: Display) = Props(new DisplayActor(display))

  case object Initialize

  case class FadeOut(time: Int, pins: Set[Int])

  case class Pulse(pins: Set[Int])

  case object Reset

  case class SetPins(on: Set[Int])

}

case class DisplayActor(display: Display) extends Actor with ActorLogging {

  implicit val node = Cluster(context.system)

  override def receive: Receive = {
    case Initialize => display.cyclePinsFadeOut()
    case pulse: Pulse => display.pulse(pulse.pins)
    case Reset => display.off(Set(Pins.Red, Pins.Green, Pins.Blue))
    case SetPins(pins) => display.off(Set(Pins.Red, Pins.Green, Pins.Blue)).flatMap { _ =>
      display.on(pins)
    }
    case Blink(times) => display.blink(times, Set(Pins.Red, Pins.Green, Pins.Blue))
    case FadeOut(time, pins) => display.fadeOut(time, pins)
  }
}