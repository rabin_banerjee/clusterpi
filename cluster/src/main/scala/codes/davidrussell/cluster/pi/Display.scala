package codes.davidrussell.cluster.pi

import scala.concurrent.Future

trait Display {
  def on(pins: Set[Int]): Future[Unit]
  def off(pins: Set[Int]): Future[Unit]
  def fadeOut(duration: Int, pins: Set[Int]): Future[Unit]
  def fadeIn(duration: Int, pins: Set[Int]): Future[Unit]
  def pulse(pins: Set[Int]): Future[Unit]
  def stopPulse()
  def blink(times: Int, pins: Set[Int]): Future[Unit]
  def cyclePinsFadeOut(): Future[Unit]
}