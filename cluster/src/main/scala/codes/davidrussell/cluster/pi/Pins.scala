package codes.davidrussell.cluster.pi

object Pins {
  val Red = 0
  val Green = 1
  val Blue = 2

  def getText(pin: Int) = {
    pin match {
      case Green => "Green"
      case Blue => "Blue"
      case _ => "Red"
    }
  }
}