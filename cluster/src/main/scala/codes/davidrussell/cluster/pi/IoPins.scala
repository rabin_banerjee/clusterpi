package codes.davidrussell.cluster.pi

import com.pi4j.io.gpio.GpioFactory
import com.pi4j.wiringpi.{Gpio, SoftPwm}

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class IoPins() extends Display {

  lazy private val gpio = GpioFactory.getInstance()

  private val pwmRange = 100
  private val sleepTime = 50

  private var pulsing = false

  Gpio.wiringPiSetup()
  SoftPwm.softPwmCreate(Pins.Red, 0, pwmRange)
  SoftPwm.softPwmCreate(Pins.Green, 0, pwmRange)
  SoftPwm.softPwmCreate(Pins.Blue, 0, pwmRange)

  def on(pins: Set[Int]) = Future {
    pins.foreach(x => SoftPwm.softPwmWrite(x, 100))
  }

  def off(pins: Set[Int]) = Future {
    stopPulse()
    pins.foreach(x => SoftPwm.softPwmWrite(x, 0))
  }

  def fadeOut(duration: Int, pins: Set[Int]) = Future {
    pins.foreach(SoftPwm.softPwmWrite(_, 100))
    val decrement = pwmRange / (duration / sleepTime)
    for (x <- pwmRange to 0 by -decrement) {
      pins.foreach(SoftPwm.softPwmWrite(_, x))
      Thread.sleep(sleepTime)
    }
  }

  def fadeIn(duration: Int, pins: Set[Int]) = Future {
    pins.foreach(SoftPwm.softPwmWrite(_, 0))
    val step = pwmRange / (duration / sleepTime)
    for (x <- 0 to pwmRange by step) {
      pins.foreach(SoftPwm.softPwmWrite(_, x))
      Thread.sleep(sleepTime)
    }
  }

  override def cyclePinsFadeOut(): Future[Unit] = Future {
    for {
      _ <- fadeOut(500, Set(Pins.Red))
      _ <- fadeOut(500, Set(Pins.Green))
    } yield fadeOut(500, Set(Pins.Blue))
  }

  def pulse(pins: Set[Int]) = Future {
    pulsing = true
    doPulse(pins)
  }

  def stopPulse() = {
    pulsing = false
  }

  private def doPulse(pins: Set[Int]): Future[Unit] = Future {
    if (pulsing) {
      fadeIn(duration = 250, pins)
        .map(_ => Thread.sleep(250))
        .map(_ => fadeOut(250, pins))
        .map(_ => Thread.sleep(250))
        .map(_ => doPulse(pins))
    }
  }

  override def blink(times: Int, pins: Set[Int]) = Future {
    (1 to times).foreach(_ => {
      on(pins)
      Thread.sleep(250)
      off(pins)
      Thread.sleep(250)
    })
  }
}