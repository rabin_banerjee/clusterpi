package codes.davidrussell.cluster.pi

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class ConsoleLog() extends Display {

  var pulsing = false

  override def fadeOut(duration: Int, pins: Set[Int]): Future[Unit] = Future {
    println(s"Fadeout ${pins.mkString(",")}")
  }

  override def on(pins: Set[Int]): Future[Unit] = Future {
    println(s"Pins ${pins.mkString(",")} on")
  }

  override def off(pins: Set[Int]): Future[Unit] = Future {
    stopPulse()
    println(s"Pins ${pins.mkString(",")} off")
  }

  override def fadeIn(duration: Int, pins: Set[Int]): Future[Unit] = Future {
    println(s"Fadein ${pins.mkString(",")}")
  }

  def pulse(pins: Set[Int]) = Future {
    pulsing = true
    doPulse(pins)
  }

  def stopPulse() = {
    pulsing = false
  }

  private def doPulse(pins: Set[Int]): Future[Unit] = Future {
    if (pulsing) {
      fadeIn(duration = 1000, pins)
        .map(_ => Thread.sleep(1000))
        .map(_ => fadeOut(1000, pins))
        .map(_ => Thread.sleep(1000))
        .map(_ => doPulse(pins))
    }
  }

  override def blink(times: Int, pins: Set[Int]) = Future {
    (1 to times).foreach(_ => {
      on(pins)
      Thread.sleep(250)
      off(pins)
      Thread.sleep(250)
    })
  }

  override def cyclePinsFadeOut(): Future[Unit] = Future {
    for {
      _ <- fadeOut(500, Set(Pins.Red))
      _ <- fadeOut(500, Set(Pins.Green))
    } yield fadeOut(500, Set(Pins.Blue))
  }
}