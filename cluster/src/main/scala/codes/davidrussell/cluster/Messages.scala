package codes.davidrussell.cluster

import akka.actor.ActorRef

object Messages {
  final case class Result(n: String)
  final case class ModeChanged(mode: Mode)
  final case class SetMode(mode: Mode)
  case object GetMode
  final case class Vote(color: Int)
  final case class VoteResult(result: List[Int])
  case object VoteReset
  case class VoteCount(replyTo: ActorRef)
  final case class Blink(n: Int)
}